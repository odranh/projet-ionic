angular.module('app')
.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider

        .state('countriesListCtrl', {
            url: '/',
            controller: 'countriesListCtrl',
            templateUrl: '/templates/countries/countries.html'
        })
        .state('countryDetail', {
            url: '/country/:iso2',
            controller: 'countryDetailController',
            templateUrl: '/templates/countries/contracts.html'
        })
        .state('contractDetail', {
            url: 'stations/:id',
            controller: 'contractDetailController',
            templateUrl: '/templates/countries/stations.html'
        })
        .state('GeoCtrl', {
            url: '/geolocalisation',
            controller: 'GeoCtrl',
            templateUrl: '/templates/countries/geolocalisation.html'
        });
});
