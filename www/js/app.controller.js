angular.module('app')
.controller('countriesListCtrl', function($scope,countriesService) {
  countriesService.getList()
  .then(function(response) {
    $scope.countries = response.data;
  })
  .catch(function(error){
    console.error(error.message);
  })
})
.controller('countryDetailController', function($scope,$stateParams,countryDetailService) {


  countryDetailService.getList($stateParams.iso2)
  .then(function(response) {
    $scope.contracts = response.data;
  })
  .catch(function(error){
    console.error(error.message);
  })

})
.controller('contractDetailController', function($scope,$stateParams,contractDetailService) {
  function update() {
    console.log('refresh');
    contractDetailService.getList($stateParams.id)
    .then(function(response) {
      $scope.stations = response.data;
    })
    .catch(function(error){
      console.error(error.message);
    })
  }
  update();
  setInterval(update(),1000);

})
.controller('GeoCtrl', function ($scope, $cordovaGeolocation, contractsList,contractDetailService) {
  console.log('Début géolocalisation');
  var posOptions = {timeout: 10000, enableHighAccuracy: false};
  var latpos, long;
  var smallest_distance=0;
  var contract;
  $cordovaGeolocation
      .getCurrentPosition(posOptions)
      .then(function (position) {
          latpos = position.coords.latitude;
          long = position.coords.longitude;
          console.log('Position trouvée : ('+latpos+','+long+')');
          contractsList.getList()
              .then(function (response) {
                  $scope.contracts = response.data;
                  angular.forEach($scope.contracts, function (value, key) {
                      // Code piqué sur stack overflow pour calculer la distance
                      var R = 6371; // Radius of the earth in km
                      var dLat = deg2rad(latpos - value.latitude);  // deg2rad below
                      var dLon = deg2rad(long - value.longitude);
                      var a =
                              Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                              Math.cos(deg2rad(latpos)) * Math.cos(deg2rad(value.latitude)) *
                              Math.sin(dLon / 2) * Math.sin(dLon / 2)
                          ;
                      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                      var d = R * c; //Distance en km

                      //Enregistrement de la plus petite distance
                      if (smallest_distance == 0) {
                        smallest_distance = d;
                        contract = value;
                      }
                      if(smallest_distance > d) {
                        smallest_distance = d;
                        contract = value;
                      }

                      console.log('Le plus proche en km : '+smallest_distance+" Nom contract : "+contract.name);

                      function deg2rad(deg) {
                          return deg * (Math.PI / 180)
                      }
                  })
                  //Enregistrement du contract pour centrer la carte
                  $scope.contract = contract;
                  //Récupération de toutes les stations
                  contractDetailService.getList(contract.name)
                  .then(function(response) {
                    $scope.stations = response.data;
                  })
                  .catch(function(error){
                    console.error(error.message);
                  })

              })
              .catch(function (error) {
                  console.error(error.message);
              })
      }, function (err) {
      });
});
