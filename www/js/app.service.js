angular.module('app')
.service('countriesService',function($http,apiHost) {
  this.getList = function() {
    return $http.get(apiHost+ 'countries');
  }
})
.service('countryDetailService',function($http,apiHost) {
  this.getList = function(iso2) {
    return $http.get(apiHost+ 'countries/'+iso2+"/contracts");
  }
})
.service('contractDetailService',function($http,apiHost) {
  this.getList = function(id) {
    return $http.get(apiHost+ "contracts/"+id+"/stations");
  }
})
.service('contractsList',function($http,apiHost) {
  this.getList = function() {
    return $http.get(apiHost+ "contracts");
  }
})
